<?php
/* vim: set noexpandtab tabstop=2 softtabstop=2 shiftwidth=2: */

// Updated by Xymph

/**
 * Structure of a Record.
 */
class Record {
	public $player;
	public $challenge;
	public $score;
	public $date;
	public $checks;
	public $new;
	public $pos;
}  // class Record

/**
 * Manages a list of records.
 * Add records to the list and remove them.
 */
class RecordList {
	public $record_list;

	// instantiates a record list with max $limit records
	function __construct(public $max) {
		$this->record_list = [];
	}

	function setLimit($limit) {
		$this->max = $limit;
	}

	function getRecord($rank) {
		if (isset($this->record_list[$rank]))
			return $this->record_list[$rank];
		else
			return false;
	}

	function setRecord($rank, $record) {
		if (isset($this->record_list[$rank])) {
			return $this->record_list[$rank] = $record;
		} else {
			return false;
		}
	}

	function moveRecord($from, $to) {
		moveArrayElement($this->record_list, $from, $to);
	}

	function addRecord($record, $rank = -1) {

		// if no rank was set for this record, then put it to the end of the list
		if ($rank == -1) {
			$rank = count($this->record_list);
		}

		// do not insert a record behind the border of the list
		if ($rank >= $this->max) return;

		// do not insert a record with no score
		if ($record->score <= 0) return;

		// if the given object is a record
		if ($record::class == 'Record') {

			// if records are getting too much, drop the last from the list
			if (count($this->record_list) >= $this->max) {
				array_pop($this->record_list);
			}

			// insert the record at the specified position
			return insertArrayElement($this->record_list, $record, $rank);
		}
	}

	function delRecord($rank = -1) {

		// do not remove a record outside the current list
		if ($rank < 0 || $rank >= count($this->record_list)) return;

		// remove the record from the specified position
		return removeArrayElement($this->record_list, $rank);
	}

	function count() {
		return count($this->record_list);
	}

	function clear() {
		$this->record_list = [];
	}
}  // class RecordList


/**
 * Structure of a Player.
 * Can be instantiated with an RPC 'GetPlayerInfo' or
 * 'GetDetailedPlayerInfo' response.
 */
class Player {
	public $id;
	public $pid;
	public $login;
	public $nickname;
	public $teamname;
	public $ip;
	public $client;
	public $ipport;
	public $zone;
	public $nation;
	public $prevstatus;
	public $isspectator;
	public $isofficial;
	public $rights;
	public $language;
	public $avatar;
	public $teamid;
	public $unlocked;
	public $ladderrank;
	public $ladderscore;
	public $created;
	public $wins;
	public $newwins;
	public $timeplayed;
	public $tracklist;
	public $playerlist;
	public $msgs;
	public $pmbuf;
	public $mutelist;
	public $mutebuf;
	public $style;
	public $panels;
	public $speclogin;
	public $dedirank;

	function getWins() {
		return $this->wins + $this->newwins;
	}

	function getTimePlayed() {
		return $this->timeplayed + $this->getTimeOnline();
	}

	function getTimeOnline() {
		return $this->created > 0 ? time() - $this->created : 0;
	}

	// instantiates the player with an RPC response
	function __construct($rpc_infos = null) {
		$this->id = 0;
		if ($rpc_infos) {
			$this->pid = $rpc_infos['PlayerId'];
			$this->login = $rpc_infos['Login'];
			$this->nickname = $rpc_infos['NickName'];
			$this->ipport = $rpc_infos['IPAddress'];
			$this->ip = preg_replace('/:\d+/', '', (string) $rpc_infos['IPAddress']);  // strip port
			$this->prevstatus = false;
			$this->isspectator = $rpc_infos['IsSpectator'];
			$this->isofficial = $rpc_infos['IsInOfficialMode'];
			$this->teamname = $rpc_infos['LadderStats']['TeamName'];
			if (isset($rpc_infos['Nation'])) {  // TMN (TMS/TMO?)
				$this->zone = $rpc_infos['Nation'];
				$this->nation = $rpc_infos['Nation'];
				$this->ladderrank = $rpc_infos['LadderStats']['Ranking'];
				$this->ladderscore = $rpc_infos['LadderStats']['Score'];
				$this->client = '';
				$this->rights = false;
				$this->language = '';
				$this->avatar = '';
				$this->teamid = 0;
			} else {  // TMF
				$this->zone = substr((string) $rpc_infos['Path'], 6);  // strip 'World|'
				$this->nation = explode('|', (string) $rpc_infos['Path']);
				if (isset($this->nation[1]))
					$this->nation = $this->nation[1];
				else
					$this->nation = '';
				$this->ladderrank = $rpc_infos['LadderStats']['PlayerRankings'][0]['Ranking'];
				$this->ladderscore = round($rpc_infos['LadderStats']['PlayerRankings'][0]['Score'], 2);
				$this->client = $rpc_infos['ClientVersion'];
				$this->rights = ($rpc_infos['OnlineRights'] == 3);  // United = true
				$this->language = $rpc_infos['Language'];
				$this->avatar = $rpc_infos['Avatar']['FileName'];
				$this->teamid = $rpc_infos['TeamId'];
			}
			$this->created = time();
		} else {
			// set defaults
			$this->pid = 0;
			$this->login = '';
			$this->nickname = '';
			$this->ipport = '';
			$this->ip = '';
			$this->prevstatus = false;
			$this->isspectator = false;
			$this->isofficial = false;
			$this->teamname = '';
			$this->zone = '';
			$this->nation = '';
			$this->ladderrank = 0;
			$this->ladderscore = 0;
			$this->rights = false;
			$this->created = 0;
		}
		$this->wins = 0;
		$this->newwins = 0;
		$this->timeplayed = 0;
		$this->unlocked = false;
		$this->pmbuf = [];
		$this->mutelist = [];
		$this->mutebuf = [];
		$this->style = [];
		$this->panels = [];
		$this->speclogin = '';
		$this->dedirank = 0;
		$this->tracklist = [];
	}
}  // class Player

/**
 * Manages players on the server.
 * Add player and remove them.
 */
class PlayerList {
	public $player_list;

	// instantiates the empty player list
	function __construct() {
		$this->player_list = [];
	}

	function nextPlayer() {
		if (is_array($this->player_list)) {
			$player_item = current($this->player_list);
			next($this->player_list);
			return $player_item;
		} else {
			$this->resetPlayers();
			return false;
		}
	}

	function resetPlayers() {
		if (is_array($this->player_list)) {
			reset($this->player_list);
		}
	}

	function addPlayer($player) {
		if ($player::class == 'Player' && $player->login != '') {
			$this->player_list[$player->login] = $player;
			return true;
		} else {
			return false;
		}
	}

	function removePlayer($login) {
		if (isset($this->player_list[$login])) {
			$player = $this->player_list[$login];
			unset($this->player_list[$login]);
		} else {
			$player = false;
		}
		return $player;
	}

	function getPlayer($login) {
		if (isset($this->player_list[$login]))
			return $this->player_list[$login];
		else
			return false;
	}
}  // class PlayerList


/**
 * Can store challenge information.
 * You can instantiate with an RPC 'GetChallengeInfo' response.
 */
class Challenge {
	public $id;
	public $name;
	public $uid;
	public $filename;
	public $author;
	public $environment;
	public $mood;
	public $bronzetime;
	public $silvertime;
	public $goldtime;
	public $authortime;
	public $copperprice;
	public $laprace;
	public $forcedlaps;
	public $nblaps;
	public $nbchecks;
	public $score;
	public $starttime;
	public $gbx;
	public $tmx;

	// instantiates the challenge with an RPC response
	function __construct($rpc_infos = null) {
		$this->id = 0;
		if ($rpc_infos) {
			$this->name = stripNewlines($rpc_infos['Name']);
			$this->uid = $rpc_infos['UId'];
			$this->filename = $rpc_infos['FileName'];
			$this->author = $rpc_infos['Author'];
			$this->environment = $rpc_infos['Environnement'];
			$this->mood = $rpc_infos['Mood'];
			$this->bronzetime = $rpc_infos['BronzeTime'];
			$this->silvertime = $rpc_infos['SilverTime'];
			$this->goldtime = $rpc_infos['GoldTime'];
			$this->authortime = $rpc_infos['AuthorTime'];
			$this->copperprice = $rpc_infos['CopperPrice'];
			$this->laprace = $rpc_infos['LapRace'];
			$this->forcedlaps = 0;
			if (isset($rpc_infos['NbLaps']))
				$this->nblaps = $rpc_infos['NbLaps'];
			else
				$this->nblaps = 0;
			if (isset($rpc_infos['NbCheckpoints']))
				$this->nbchecks = $rpc_infos['NbCheckpoints'];
			else
				$this->nbchecks = 0;
		} else {
			// set defaults
			$this->name = 'undefined';
		}
	}
}  // class Challenge


/**
 * Contains information about an RPC call.
 */
class RPCCall {
	// instantiates the RPC call with the parameters
	function __construct(public $id, public $index, public $callback, public $call)
 {
 }
}  // class RPCCall


/**
 * Contains information about a chat command.
 */
class ChatCommand {
	// instantiates the chat command with the parameters
	function __construct(public $name, public $help, public $isadmin)
 {
 }
}  // class ChatCommand


/**
 * Stores basic information of the server XASECO is running on.
 */
class Server {
	public $id;
	public $name;
	public $game;
	public $serverlogin;
	public $nickname;
	public $zone;
	public $rights;
	public $timeout;
	public $version;
	public $build;
	public $packmask;
	public $laddermin;
	public $laddermax;
	public $maxplay;
	public $maxspec;
	public $challenge;
	public $records;
	public $players;
	public $mutelist;
	public $gamestate;
	public $gameinfo;
	public $gamedir;
	public $trackdir;
	public $votetime;
	public $voterate;
	public $uptime;
	public $starttime;
	public $isrelay;
	public $relaymaster;
	public $relayslist;

	// game states
	public const RACE  = 'race';
	public const SCORE = 'score';

	function getGame() {
		return match ($this->game) {
      'TmForever' => 'TMF',
      'TmNationsESWC' => 'TMN',
      'TmSunrise' => 'TMS',
      'TmOriginal' => 'TMO',
      default => 'Unknown',
  };
	}

	// instantiates the server with default parameters
	function __construct(public $ip, public $port, public $login, public $pass) {
		$this->starttime = time();
	}
}  // class Server

/**
 * Contains information to the current game which is played.
 */
class Gameinfo {
	public $mode;
	public $numchall;
	public $rndslimit;
	public $timelimit;
	public $teamlimit;
	public $lapslimit;
	public $cuplimit;
	public $forcedlaps;

	public const RNDS = 0;
	public const TA   = 1;
	public const TEAM = 2;
	public const LAPS = 3;
	public const STNT = 4;
	public const CUP  = 5;

	// returns current game mode as string
	function getMode() {
		return match ($this->mode) {
      self::RNDS => 'Rounds',
      self::TA => 'TimeAttack',
      self::TEAM => 'Team',
      self::LAPS => 'Laps',
      self::STNT => 'Stunts',
      self::CUP => 'Cup',
      default => 'Undefined',
  };
	}

	// instantiates the game info with an RPC response
	function __construct($rpc_infos = null) {
		if ($rpc_infos) {
			$this->mode = $rpc_infos['GameMode'];
			$this->numchall = $rpc_infos['NbChallenge'];
			if (isset($rpc_infos['RoundsUseNewRules']) && $rpc_infos['RoundsUseNewRules'])
				$this->rndslimit = $rpc_infos['RoundsPointsLimitNewRules'];
			else
				$this->rndslimit = $rpc_infos['RoundsPointsLimit'];
			$this->timelimit = $rpc_infos['TimeAttackLimit'];
			if (isset($rpc_infos['TeamUseNewRules']) && $rpc_infos['TeamUseNewRules'])
				$this->teamlimit = $rpc_infos['TeamPointsLimitNewRules'];
			else
				$this->teamlimit = $rpc_infos['TeamPointsLimit'];
			$this->lapslimit = $rpc_infos['LapsTimeLimit'];
			if (isset($rpc_infos['CupPointsLimit']))
				$this->cuplimit = $rpc_infos['CupPointsLimit'];
			if (isset($rpc_infos['RoundsForcedLaps']))
				$this->forcedlaps = $rpc_infos['RoundsForcedLaps'];
			else
				$this->forcedlaps = 0;
		} else {
			$this->mode = -1;
		}
	}
}  // class Gameinfo
?>
