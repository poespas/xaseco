# xaseco

Server controller for TrackMania Forever, updated to be compatible with PHP 8

## Plugins
The following plugins have already been updated to work with PHP 8, some contain a couple bug fixes and/or performance improvements as well.

- [BestCPs](https://codeberg.org/hanouta/xaseco-plugin-bestcps) (Note: this plugin is a complete rewrite, and also the config file has been changed slightly, so you can't just keep using your existing bestcps.xml config)
- [Fufi AutoQueue](https://codeberg.org/hanouta/xaseco-plugin-fufi-autoqueue)
- [Fufi Menu](https://codeberg.org/hanouta/xaseco-plugin-fufi-menu)
- [ManiaKarma](https://codeberg.org/hanouta/xaseco-plugin-mania-karma)
- [Nickname Sync](https://codeberg.org/hanouta/xaseco-plugin-nickname-sync)
- [Pay2Play](https://codeberg.org/hanouta/xaseco-plugin-pay2play)
- [Records Eyepiece](https://codeberg.org/hanouta/xaseco-plugin-records-eyepiece)
- [SecRecs/BestSecs](https://codeberg.org/hanouta/xaseco-plugin-secrecs)
- [Server Neighborhood](https://codeberg.org/hanouta/xaseco-plugin-server-neighborhood)
- [Nouse Betting](https://codeberg.org/hanouta/xaseco-plugin-nouse-betting)
- [Nouse Winning](https://codeberg.org/hanouta/xaseco-plugin-nouse-winning)
- [Bueddl Karma Widget](https://codeberg.org/hanouta/xaseco-plugin-bueddl-karma-widget)
- [Spyke AllCps](https://codeberg.org/hanouta/xaseco-plugin-spyke-allcps)
- [Freezone](https://codeberg.org/hanouta/xaseco-plugin-freezone)

Other plugins may or may not work without modifications, if there is a plugin which doesn't work out of the box, and you need help in making it work, create an issue in the issue tracker here.

## Required PHP Extensions
- mbstring
- mysql
- xml

When using the Freezone Plugin, the curl Extension is required as well.